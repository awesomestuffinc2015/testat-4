#include <cstdlib>
#include <iostream>
#include <ctime>
#include <vector>
#include <list>
#include "BinaryOctet.hpp"
#include <array>

struct Order {
  bool operator()(BinaryOctet &a, BinaryOctet &b) const {
    return a.binaryToInt() < b.binaryToInt();
  }
};

int findSimilarLetters(std::string a, std::string b) {
   int similarLetters = 0;
   for(char c : a) {
       if(b.find(c) < b.length()) {
           similarLetters++;
       }
   }
   return similarLetters;
}

int findSimilarLettersOnPos(std::string a, std::string b) {
   int length = 0;
   int similarLetters = 0;
   length = (a.length() < b.length() ? a.length() : b.length());

   for(int i = 0; i<length; i++) {
       if(a[i] == b[i]) {
           similarLetters++;
       }
   }
   return similarLetters;
}

int numberOfUpperCaseChars(std::string str) {
   std::string returnStr = "";
   char newChar;
   for (char c : str) {
       newChar = toupper(c);
       if(c != newChar) {
           returnStr += newChar;
       }
   }
   return findSimilarLetters(str, returnStr);
}

int stringSimilarity(std::string a, std::string b) {
   int similar = 0;
   int shortestLength, longestLength, similarUpperCase, similarLowerCase;
   int similarLength = 0;
   if(a == b) {
       return 100;
   }
   if(a.length() < b.length()) {
       longestLength = b.length();
       shortestLength = a.length();
   } else {
       longestLength = a.length();
       shortestLength = b.length();
   }
   // Similar letters in percent
   int similarLettersOnPos = findSimilarLettersOnPos(a,b)*100 / longestLength;
   int similarLetters = findSimilarLetters(a,b)*100 / longestLength;

   if(numberOfUpperCaseChars(a) < numberOfUpperCaseChars(b)) {
       similarUpperCase = numberOfUpperCaseChars(a);
       similarLowerCase = (shortestLength - numberOfUpperCaseChars(a) ) * 100 / longestLength;
   } else {
       similarUpperCase = numberOfUpperCaseChars(b) * 100 / longestLength;
       similarLowerCase = (shortestLength - numberOfUpperCaseChars(b) ) * 100 / longestLength;
   }
   if(a.length() == b.length()) {
       similarLength = 100;
   }
   similar = (4*similarLettersOnPos + (3*similarLetters ) +  (similarLength) + (similarUpperCase) + (similarLowerCase))/10;
   return similar;
   
}

void unittest_stringSimilarity() {
    std::string a = "ABC";
    std::string b = "abc";
    std::string c = "ABC";
    std::string d = "BCA";
    std::string e = "abcEFG";
    std::string f = "EFG";
    std::string g = "   ";
    std::cout << "a: " << a << std::endl;
    std::cout << "b: " << b << std::endl;
    std::cout << "c: " << c << std::endl;
    std::cout << "d: " << d << std::endl;
    std::cout << "e: " << e << std::endl;
    std::cout << "f: " << f << std::endl;
    std::cout << "g: " << g << std::endl;
    std::cout << "a and b: " << stringSimilarity(a,b) << std::endl;
    std::cout << "a and a: "<< stringSimilarity(a,a) << std::endl;
    std::cout << "c and d: " << stringSimilarity(c,d) << std::endl;
    std::cout << "e and f: " << stringSimilarity(e, f) << std::endl;
    std::cout << "g and a: "<< stringSimilarity(g, a) << std::endl;
}

void fillVector(std::vector<int> &intVector, int *randoms) {
    for (int i = 0; i < 23; i++) {
        for (int j = 0; j < 42; j++ ) {
            if (randoms[j] == i) {
                intVector.push_back(randoms[j]);
            }
        }
    }

}

void fillList(std::list<int> &intList, int *randoms) {
    std::list<int>::iterator iter;
    bool flag = false;
    

    for (int i = 0; i < 42; i++) {
        flag = false;

        if (intList.size() == 0) {
            intList.push_back(randoms[i]);
        } else {
            for (iter = intList.begin(); iter != intList.end(); iter++) {
                if (*iter == randoms[i]) {
                    flag = true;
                }
            }

            if (!flag) {
                intList.push_back(randoms[i]);
            }
        }
    }

}

int main() {
    const int size = 42;
    const int min = 0;
    const int max = 23;
    int random_variable = 0;
    int rand_vars[size];
    std::srand(std::time(0));
    std::vector<int> intVector;
    std::list<int> intList;

    int post;

    for (int i = 0; i < size; i++) {
        random_variable = std::rand()%(max - min + 1) + min;
        rand_vars[i] = random_variable;

    }


    fillVector(intVector, rand_vars);


    std::vector<int>::iterator iter;
    std::cout << "Vector: " << '\n';
    for (iter = intVector.begin(); iter != intVector.end(); iter++) {
        std::cout << *iter << " ";
    }
    std::cout << '\n';

    std::cout << "List: " << '\n';
    fillList(intList, rand_vars);
    std::list<int>::iterator listIter;
    for (listIter = intList.begin(); listIter != intList.end(); listIter++) {
        std::cout << *listIter << " ";
    }
}
