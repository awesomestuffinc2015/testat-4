#include <iostream>
#include <assert.h>
#include <cmath>
#include "BinaryOctet.hpp"

char cStringArea[1024];

void BinaryOctet::setParity() {
    int count = 0;
    for (auto c : bitsAsDigits) {
        if (c == '1') {
            count++;
        }
    }

    BinaryOctet::evenParity = (count % 2 == 0);
}


int BinaryOctet::binaryToInt() {
    int returnValue = 0;
    double exponent = 0;

    for (int i = bitsPerOctet - 1; i > 0; --i) {
        if (bitsAsDigits[i] == '0') {
            returnValue = returnValue + 0;
        } else if (bitsAsDigits[i] == '1') {
            returnValue = (int) (returnValue + pow(2.0, exponent));
        }
        ++exponent;
    }

    return returnValue;
}

void BinaryOctet::make2Komplement(BinaryOctet &x) {
    int digit = 0;
    for (int i = 0; i < bitsPerOctet; ++i) {
        digit = (int) x.bitsAsDigits[i];
        digit == 1 ? x.bitsAsDigits[i] = '0' : x.bitsAsDigits[i] = '1';
    }
    x = x + 1;
}

std::ostream &operator<<(std::ostream &os, const BinaryOctet &toBePrinted) {
    for (auto c : toBePrinted.bitsAsDigits)os << c;
    os << " Parity: " << toBePrinted.evenParity;
    return os;
}

BinaryOctet BinaryOctet::add(BinaryOctet right) {

    int leftBit;
    int rightBit;
    int cache = 0;
    int carry = 0;
    BinaryOctet result(0);

    for (int i = 7; i >= 0; i--) {
        leftBit = (bitsAsDigits[i] - '0');
        rightBit = (right.bitsAsDigits[i] - '0');

        cache = (leftBit ^ rightBit ^ carry) + '0';
        result.bitsAsDigits[i] = (char) cache;
        carry = (leftBit & rightBit) | (rightBit & carry) | (leftBit & carry);
    }

    assert(carry == 0);
    result.setParity();
    return result;
}

BinaryOctet BinaryOctet::operator+(int intRight) {
    BinaryOctet right = BinaryOctet(intRight);
    return add(right);
}

BinaryOctet BinaryOctet::operator+(BinaryOctet right) {
    return add(right);
}

BinaryOctet BinaryOctet::operator/(int right) {
    int intLeft = binaryToInt();
    return BinaryOctet(intLeft / right);
}

bool BinaryOctet::operator&&(BinaryOctet right) {

    for (int i = 0; i < bitsPerOctet; ++i) {
        if (bitsAsDigits[i] != '1' || right.bitsAsDigits[i] != '1') {
            return false;
        }
    }

    return true;
}

BinaryOctet BinaryOctet::operator-(BinaryOctet right) {
    BinaryOctet result(0);

    BinaryOctet::make2Komplement(right);

    result = *this + right;

    return result;
}

BinaryOctet BinaryOctet::operator--(int right) {
    BinaryOctet result(0);
    BinaryOctet subtractor(0);
    subtractor = BinaryOctet(right);

    result = *this - subtractor;

    return result;
}

BinaryOctet::BinaryOctet(int n) {
    int iterator = 0;
    int i = 0;
    int rest = 0;

    for (int x = 0; x < bitsPerOctet; x++) {
        this->bitsAsDigits[x] = '0';
    }

    if (n == 0) {
        evenParity = false;
        return;
    }

    while (n > 0) {
        rest = n % 2;
        n = n / 2;

        i = bitsPerOctet - iterator - 1;
        this->bitsAsDigits[i] = (rest == 0 ? '0' : '1');
        iterator++;
    }

    setParity();

}

BinaryOctet BinaryOctet::operator%(BinaryOctet right) {
    int intLeft = binaryToInt();
    int intRight = right.binaryToInt();
    return BinaryOctet(intLeft % intRight);
}
