#include <iostream>
#ifndef BinaryOctet_HPP
#define BinaryOctet_HPP

const int bitsPerOctet = 8;

struct BinaryOctet {
public:

    void setParity();

    int binaryToInt();

    void make2Komplement(BinaryOctet &x);

    bool evenParity;
    char bitsAsDigits[bitsPerOctet];

    BinaryOctet(int n);

    BinaryOctet operator+(int intRight);

    BinaryOctet operator+(BinaryOctet right);

    BinaryOctet add(BinaryOctet right);

    BinaryOctet operator/(int right);

    BinaryOctet operator%(BinaryOctet right);

    bool operator&&(BinaryOctet right);

    BinaryOctet operator-(BinaryOctet right);

    BinaryOctet operator--(int right);

};

std::ostream &operator<<(std::ostream &os, const BinaryOctet &toBePrinted);

#endif